// - DOMAIN
import { MVCNode } from "@innovative/domain/MVCNode.domain"

export class DockFeature extends MVCNode {
    private label: string = '/'
    private active: boolean = false
    private route: string = '/';
   
    constructor(values: Object = {}) {
        super()
        this.jsonClass = 'DockFeature'
    }

    // - G E T T E R S
    public getLabel():string{
        return this.label
    }
    public activate(): boolean {
        const result = this.active
        this.active = true
        return result
    }
    public deactivate(): boolean {
        const result = this.active
        this.active = false
        return result
    }
    public getRoute(): string {
        return this.route;
    }
    public equals(target: DockFeature): boolean {
        if (this.label != target.label) return false;
        if (this.active != target.active) return false;
        if (this.route != target.route) return false;
        return true;
    }
}
