// - CORE
import { Injectable } from '@angular/core';
// - MATERIAL
import { MatDialogConfig } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
// - DOMAIN
import { DockFeature } from './domain/DockFeature.domain';

@Injectable({
    providedIn: 'root'
})
export class DialogFactoryService {
    protected modalDialog?: MatDialogRef<any>|undefined

    constructor(public matDialog: MatDialog) { }

    public processClick(target: DockFeature): MatDialogRef<any> {
        console.log('>[DialogFactoryService.processClick]> Feature: ' + JSON.stringify(target))
        return this.modalDialog
    }
}
