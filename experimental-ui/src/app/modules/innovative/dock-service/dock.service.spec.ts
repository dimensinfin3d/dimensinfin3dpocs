// - CORE
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { Router } from '@angular/router'
import { Observable } from 'rxjs'
import { Routes } from '@angular/router';
// - TESTING
import { async, fakeAsync, tick } from '@angular/core/testing'
import { TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { Location } from "@angular/common"
// - INNOVATIVE
import { SupportIsolationService } from '@innovative/testing-support/SupportIsolation.service'
import { AssetManagerService } from '@innovative/asset-manager/AssetManager.service'
import { IRefreshable } from '@innovative/domain/interfaces/IRefreshable.interface'
// - DOMAIN
import { DockService } from './dock.service'
import { DockFeature } from './domain/DockFeature.domain'

const routes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
]

describe('service DockService [Module: SERVICES]', () => {
    let service: DockService
    let routerDetector: any = { refresh: () => { } }
    let location: Location
    let router: Router
    let isolationService: SupportIsolationService = new SupportIsolationService()
    let clientWrapperService = {
        wrapLocalRESOURCECall: () => {
            console.log('DockService->spyOn.wrapHttpRESOURCECall')
            return new Observable(observer => {
                setTimeout(() => {
                    observer.next(isolationService.directAccessAssetResource('properties/config/DefaultDockFeatureMap'))
                }, 100)
            })
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            schemas: [NO_ERRORS_SCHEMA],
            imports: [
                RouterTestingModule.withRoutes(routes),
            ],
            declarations: [
            ],
            providers: [
                { provide: AssetManagerService, useValue: clientWrapperService }
            ]
        }).compileComponents()

        service = TestBed.inject(DockService)
        location = TestBed.inject(Location)
        router = TestBed.inject(Router)
        router.initialNavigation()
    }))

    // - C O N S T R U C T I O N   P H A S E
    describe('Construction Phase', () => {
        it('Should be created', () => {
            expect(service).toBeDefined('component has not been created.')
        })
        it('Initial state', () => {
            expect(service).toBeTruthy('service has not been created.')
            const serviceAsAny = service as any
            expect(serviceAsAny.activeFeature).toBeUndefined()
            expect(serviceAsAny.featureList).toBeUndefined()
            expect(serviceAsAny.routedComponent).toBeUndefined()
        })
    })

    // - C O D E   C O V E R A G E   P H A S E
    describe('Code Coverage Phase [DOCK]', () => {
        it('readDockConfiguration: read and return the list of configured Features', fakeAsync(() => {
            const serviceAsAny = service as any
            expect(serviceAsAny.activeFeature).toBeUndefined()
            expect(serviceAsAny.featureList).toBeUndefined()
            spyOn(clientWrapperService, 'wrapLocalRESOURCECall').and
                .callFake(function () {
                    console.log('Code Coverage Phase [DOCK]->spyOn.wrapHttpRESOURCECall')
                    return new Observable(observer => {
                        setTimeout(() => {
                            observer.next(isolationService.directAccessAssetResource('properties/config/DefaultDockFeatureMap'))
                        }, 100)
                    })
                })
            service.readDockConfiguration()
                .subscribe((response: DockFeature[]) => {
                    expect(response).toBeDefined()
                    expect(response.length).toBe(8)
                })
            tick(1100)
            expect(serviceAsAny.featureList.length).toBe(8)
        }))
        it('activateFeature.null: no feature selected for activation', fakeAsync(() => {
            const serviceAsAny = service as any
            service.readDockConfiguration()
                .subscribe((response: DockFeature[]) => {
                    expect(response).toBeDefined()
                    expect(response.length).toBe(8)
                })
            tick(100)
            expect(serviceAsAny.featureList.length).toBe(8)
            serviceAsAny.activeFeature = serviceAsAny.featureList[3]
            expect(serviceAsAny.activeFeature).toBeDefined()
            service.activateFeature(null)
            tick(100)
            expect(serviceAsAny.activeFeature).toBeUndefined()
            expect(serviceAsAny.featureList.length).toBe(8)
            expect(location.path()).toBe('/')
        }))
        it('activateFeature.new feature: activate a new selected feature', fakeAsync(() => {
            // Given
            const serviceAsAny = service as any
            service.readDockConfiguration()
                .subscribe((response: DockFeature[]) => {
                    expect(response).toBeDefined()
                    expect(response.length).toBe(8)
                })
            tick(100)
            expect(serviceAsAny.featureList.length).toBe(8)
            expect(serviceAsAny.featureList[3]).toBeDefined()
            expect(serviceAsAny.featureList[3].active).toBeFalse()
            spyOn(serviceAsAny.featureList[3], 'activate').and.callThrough()
            expect(serviceAsAny.activeFeature).toBeUndefined('Initial state is no feature activated.')
            // Test
            service.activateFeature(serviceAsAny.featureList[3])
            tick(100)
            // Expect
            expect(serviceAsAny.activeFeature).toBeDefined()
            expect(serviceAsAny.activeFeature).toBe(serviceAsAny.featureList[3])
            expect(serviceAsAny.featureList[3].active).toBeTrue()
            expect(serviceAsAny.featureList[3].activate).toHaveBeenCalled()
            expect(location.path()).toBe('/production/pendingjobs')
        }))
        it('activateFeature.replace feature: activate a new selected feature', fakeAsync(() => {
            // Given
            const serviceAsAny = service as any
            service.readDockConfiguration()
                .subscribe((response: DockFeature[]) => {
                    expect(response).toBeDefined()
                    expect(response.length).toBe(8)
                })
            tick(100)
            expect(serviceAsAny.featureList.length).toBe(8)
            service.activateFeature(serviceAsAny.featureList[3])
            tick(100)
            expect(serviceAsAny.activeFeature).toBeDefined()
            expect(serviceAsAny.activeFeature).toBe(serviceAsAny.featureList[3])
            expect(serviceAsAny.featureList[3].active).toBeTrue()
            spyOn(serviceAsAny.featureList[1], 'activate').and.callThrough()
            // Test
            service.activateFeature(serviceAsAny.featureList[1])
            tick(100)
            // Expect
            expect(serviceAsAny.activeFeature).toBeDefined()
            expect(serviceAsAny.featureList[3].active).toBeFalse()
            expect(serviceAsAny.featureList[1].active).toBeTrue()
            expect(serviceAsAny.activeFeature).toBe(serviceAsAny.featureList[1])
            expect(location.path()).toBe('/inventory/partlist')
        }))
        it('clean.success: deactivate any feature', fakeAsync(() => {
            // Given
            const serviceAsAny = service as any
            service.readDockConfiguration()
                .subscribe((response: DockFeature[]) => {
                    expect(response).toBeDefined()
                    expect(response.length).toBe(8)
                })
            tick(100)
            expect(serviceAsAny.featureList.length).toBe(8)
            service.activateFeature(serviceAsAny.featureList[3])
            tick(100)
            expect(serviceAsAny.activeFeature).toBeDefined()
            expect(serviceAsAny.activeFeature).toBe(serviceAsAny.featureList[3])
            expect(serviceAsAny.featureList[3].active).toBeTrue()
            // Test
            service.clean()
            expect(serviceAsAny.activeFeature).toBeUndefined()
        }))
        it('activatePage.success: set the active component to have the refresh target', () => {
            // Given
            const serviceAsAny = service as any
            expect(serviceAsAny.routedComponent).toBeUndefined()
            const pageRef: IRefreshable = { clean: () => { }, refresh: () => { } }
            // Test
            service.activatePage(pageRef)
            // Expect
            expect(serviceAsAny.routedComponent).toBeDefined()
        })
        it('refresh.success: refresh the active component', () => {
            // Given
            const serviceAsAny = service as any
            const pageRef: IRefreshable = { clean: () => { }, refresh: () => { } }
            spyOn(pageRef, 'refresh')
            service.activatePage(pageRef)
            // Test
            service.refresh()
            // Expect
            expect(pageRef.refresh).toHaveBeenCalled()
        })
    })
})
