// - TESTING
import { TestBed } from '@angular/core/testing'
// - INNOVATIVE
import { SupportIsolationService } from '@innovative/testing-support/SupportIsolation.service'
// - DOMAIN
import { Feature } from './Feature.domain'

describe('CLASS Feature [Module: DOMAIN]', () => {
    let isolation: SupportIsolationService

    beforeEach(() => {
        isolation = TestBed.get(SupportIsolationService)
    })

    // - C O N S T R U C T I O N   P H A S E
    describe('Construction Phase', () => {
        it('constructor.none: validate initial state without constructor', () => {
            const instance = new Feature()
            const instanceAsAny = instance as any
            expect(instance).toBeDefined()
            expect(instanceAsAny.label).toBe('/')
            expect(instanceAsAny.active).toBeFalsy()
            expect(instanceAsAny.route).toBe('/')
        })
        it('constructor.object: validate initial state with object data', () => {
            const instance = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceAsAny = instance as any
            expect(instance).toBeDefined()
            expect(instanceAsAny.label).toBe('-TEST-LABEL-')
            expect(instanceAsAny.active).toBeTruthy()
            expect(instanceAsAny.route).toBe('-TEST-ROUTE-')
        })
    })

    // - G E T T E R   P H A S E
    describe('Getter Phase', () => {
        it('getRoute: access "route" field.', () => {
            const instance = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceAsAny = instance as any
            expect(instance).toBeDefined()
            expect(instanceAsAny.route).toBe('-TEST-ROUTE-')
            expect(instance.getRoute()).toBe('-TEST-ROUTE-')
        })
    })

    // - C O V E R A G E   P H A S E
    describe('Coverage Phase [Methods]', () => {
        it('equals.success: compare two Featured for equality.', () => {
            const instanceA = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceB = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            expect(instanceA).toBeDefined()
            expect(instanceB).toBeDefined()
            expect(instanceA.equals(instanceB)).toBeTruthy()
        })
        it('equals.failure: compare two Featured for equality.', () => {
            const instanceA = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceB = new Feature({ label: '-TEST-LABEL-NOT EQUAL-', active: true, route: '-TEST-ROUTE-' })
            expect(instanceA).toBeDefined()
            expect(instanceB).toBeDefined()
            expect(instanceA.equals(instanceB)).toBeFalse()
        })
        it('equals.failure: compare two Featured for equality.', () => {
            const instanceA = new Feature({ label: '-TEST-LABEL-', active: false, route: '-TEST-ROUTE-' })
            const instanceB = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            expect(instanceA).toBeDefined()
            expect(instanceB).toBeDefined()
            expect(instanceA.equals(instanceB)).toBeFalse()
        })
        it('equals.failure: compare two Featured for equality.', () => {
            const instanceA = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceB = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-NOT-EQUAL-' })
            expect(instanceA).toBeDefined()
            expect(instanceB).toBeDefined()
            expect(instanceA.equals(instanceB)).toBeFalse()
        })
        it('activate: activate the feature.', () => {
            const instance = new Feature({ label: '-TEST-LABEL-', active: false, route: '-TEST-ROUTE-' })
            const instanceAsAny = instance as any
            expect(instance).toBeDefined()
            expect(instanceAsAny.active).toBeFalsy()
            const obtained = instance.activate()
            expect(obtained).toBeFalsy()
            expect(instanceAsAny.active).toBeTruthy()
        })
        it('deactivate: deactivate the feature.', () => {
            const instance = new Feature({ label: '-TEST-LABEL-', active: true, route: '-TEST-ROUTE-' })
            const instanceAsAny = instance as any
            expect(instance).toBeDefined()
            expect(instanceAsAny.active).toBeTrue()
            const obtained = instance.deactivate()
            expect(obtained).toBeTrue()
            expect(instanceAsAny.active).toBeFalse()
        })
    })
})
