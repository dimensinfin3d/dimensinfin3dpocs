// - DOMAIN
import { DockFeature } from '@innovative/dock-service/domain/DockFeature.domain';
import { FeatureConverter } from '../converter/Feature.converter';
import { EInteraction } from './EInteraction.domain';

export class Feature extends DockFeature {
    public enabled: boolean = false;
    public hasMenu : boolean =false
    public interaction: EInteraction = EInteraction.PAGEROUTE;
    public modifier: string
    public dialog: string;

    constructor(values: Object = {}) {
        super();
        const convertedValues: object = new FeatureConverter().convertInstance(values);
        Object.assign(this, convertedValues);
        this.jsonClass = 'Feature';
    }
}
