// - DOMAIN
import { INode } from "./interfaces/INode.interface";
import { ICollaboration } from "./interfaces/ICollaboration.interface";
import { IExpandable } from "./interfaces/IExpandable.interface";
import { ISelectable } from "./interfaces/ISelectable.interface";

export class MVCNode implements INode, ICollaboration, IExpandable, ISelectable {
    public jsonClass: string = 'MVCNode';
    protected expanded: boolean = false;
    protected selected: boolean = false;

    constructor(values: Object = {}) {
        Object.assign(this, values)
        this.jsonClass = 'MVCNode'
        this.decode()
    }
    public decode(): void {
        console.log('>[Decode]>' + this.jsonClass)
    }

    protected isEmpty(target?: any): boolean {
        if (null == target) return true;
        if (Object.keys(target).length > 0) return false;
        return true;
    }

    // - I N O D E
    public getJsonClass(): string {
        return this.jsonClass;
    }
    public isActive(): boolean {
        return true;
    }
    // -  I C O L L A B O R A T I O N
    public collaborate2View(): ICollaboration[] {
        return [this];
    }

    // - I E X P A N D A B L E
    public isExpandable(): boolean {
        return false;
    }
    public isExpanded(): boolean {
        return this.expanded;
    }
    public collapse(): boolean {
        this.expanded = false;
        return this.isExpanded();
    }
    public expand(): boolean {
        this.expanded = true;
        return this.isExpanded();
    }
    public toggleExpanded(): void {
        this.expanded = !this.expanded;
        console.log('toggleExpanded: ' + this.expanded)
    }

    // - I S E L E C T A B L E
    public toggleSelected(): boolean {
        this.selected = !this.selected;
        return this.selected;
    }
    public isSelected(): boolean {
        if (this.selected) return true;
        else return false;
    }
    public select(): void {
        this.selected = true;
    }
    public unselect(): void {
        this.selected = false;
    }
}
