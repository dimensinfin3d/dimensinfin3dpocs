export class AppException {
    /** @deprecated */
    public status: number = 0;
    /** @deprecated */
    public statusText: string = 'Undefined Exception';
    /** @deprecated */
    public allowsRetry: boolean = false;
    public code!: string
    public title!: string
    public message: string = 'Request undefined exception:0:Undefined Exception.';
    public userMessage!: string
    public cause!: string

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

    // - B U I L D E R
    public withCode(code: string): AppException {
        if (null != code) this.code = code
        return this
    }
    public withTitle(title: string): AppException {
        if (null != title) this.title = title
        return this
    }
    public withMessage(message: string): AppException {
        if (null != message) this.message = message
        return this
    }
    public withCause(cause: string): AppException {
        if (null != cause) this.cause = cause
        return this
    }

    // - G E T T E R S
    /** If the user message is not defined the we can expect to use the exception message. */
    public getUserMessage(): string {
        if (this.isEmpty(this.userMessage)) return this.message;
        else return this.userMessage;
    }
    public setUserMessage(message: string): AppException {
        this.userMessage = message;
        return this;
    }
    /** @deprecated */
    public setRetryable(retry: boolean): AppException {
        return this
    }

    protected isEmpty(target?: any): boolean {
        if (null == target) return true;
        if (Object.keys(target).length > 0) return false;
        return true;
    }
}
