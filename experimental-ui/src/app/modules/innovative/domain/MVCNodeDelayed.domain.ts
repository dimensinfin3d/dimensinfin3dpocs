// - DOMAIN
import { MVCNode } from "./MVCNode.domain";

export class MVCNodeDelayed extends MVCNode {
    public ready: boolean = false

    constructor(values: Object = {}) {
        super(values)
    }

    public isReady(): boolean {
        return this.ready
    }
}
