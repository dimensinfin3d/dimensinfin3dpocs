# DOMAIN
# MVCNode
All elements that declare a model node on the Angular MVC framaworks should to inherit directly or indirectly from this class **MVCNode**.

The class declares the core structures and most of the common code to be used on MVC compatible model classes.

## SingleSelectionManager
### SingleSelectionManager.updateSelection
If the new target node is a new selection then it replaces the current selection.
If the new target is unselected then we should test if the selection reference (that should point to the same node) is also unselected. Is so the we clear the selection. If the selection does not change we then do not change the selection contents.
@param node the new target node
