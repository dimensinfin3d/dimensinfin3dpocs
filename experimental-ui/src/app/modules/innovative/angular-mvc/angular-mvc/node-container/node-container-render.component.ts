// - CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
// - DOMAIN
// import { ISelectable } from '@innovative/domain/interfaces/ISelectable.interface';
// import { IViewer } from '@innovative/domain/interfaces/IViewer.interface';
// import { MVCNode } from '@innovative/domain/MVCNode.domain';
// import { BackgroundEnabledComponent } from '../background-enabled/background-enabled.component';
import { BackgroundEnabledComponent } from '../background-enabled/background-enabled.component';
import { MVCNode } from '../../../domain/domain/MVCNode.domain';
import { IViewer } from '../../../domain/domain/interfaces/IViewer.interface';
import { ISelectable } from '../../../domain/domain/interfaces/ISelectable.interface';

export const DEFAULT_VARIANT: string = '-DEFAULT-'

@Component({
    selector: 'abstract-node-container',
    templateUrl: './node-container-render.component.html',
    styleUrls: ['./node-container-render.component.scss']
})
export class NodeContainerRenderComponent extends BackgroundEnabledComponent {
    @Input() container: IViewer;
    @Input() node: MVCNode;
    @Input() variant: string = DEFAULT_VARIANT;
    @Input() index: number = 1;
    @Input() selectOnHover: boolean = false

    // - G E T T E R S
    public getNode(): MVCNode {
        return this.node;
    }
    public getVariant(): string {
        return this.variant;
    }
    /**
     * Pass the container panel the node that is being entered so if there is additional data it can be exported to another panel.
     * @param target target node that is being entered by the cursor.
     */
    public mouseEnter(target: any) {
        if (this.selectOnHover) this.container.addSelection(target as ISelectable);
    }
    public toggleExpanded(): void {
        if (null != this.node) {
            if (this.node.isExpandable()) {
                this.node.toggleExpanded()
                this.container.notifyDataChanged();
            }
        }
    }
    public select(): void {
        this.node.select()
        this.container.updateSelection(this.node)
    }
    public unselect(): void {
        this.node.unselect()
        this.container.subtractSelection(this.node) 
    }
    public isExpanded(): boolean {
        if (null != this.node) return this.node.isExpanded()
        else return false
    }
    public isActive(): boolean {
        if (null != this.node) return this.node.isActive();
        else return true;
    }
    public isEmpty(target?: any): boolean {
        if (null == target) return true;
        if (Object.keys(target).length == 0) return true;
        return false;
    }
}
