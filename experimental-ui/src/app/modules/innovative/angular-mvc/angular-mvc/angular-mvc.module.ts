// - CORE MODULES
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
// - COMPONENTS
import { BackgroundEnabledComponent } from './background-enabled/background-enabled.component'
import { AppPanelComponent } from './app-panel/app-panel.component'
import { NodeContainerRenderComponent } from './node-container/node-container-render.component'

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        BackgroundEnabledComponent,
        AppPanelComponent,
        NodeContainerRenderComponent
    ],
    exports: [
        BackgroundEnabledComponent,
        AppPanelComponent,
        NodeContainerRenderComponent
    ]
})
export class AngularMVCModule { }
