// - CORE
import { Component } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'background-enabled',
    templateUrl: './empty.html'
})
export class BackgroundEnabledComponent implements OnDestroy {
    protected backendConnections: Subscription[] = [];
    /**
     * Unsubscribe from any open subscription made to the backend.
     */
    public ngOnDestroy(): void {
        this.backendConnections.forEach(element => {
            element.unsubscribe();
        });
    }
}
