//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
// import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
// import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
// import { IColored } from 'app/interfaces/IColored.interface';
// import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { MenuItem } from 'app/models/ui/MenuItem.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';

@Component({
  selector: 'panel-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent {
  @Input() menuContainer: IMenuBar;
  public menuItems: IMenuItem[] = [];
  public menuSections: MenuSection[] = [];
  private _isMenuActive: boolean = false;

  constructor(protected appModelStore: AppModelStoreService) { }
  ngOnInit() {
    // // Load the item contents to the public container.
    // if (null != this.menuContainer) {
    //   this.menuItems = this.menuContainer.getMenuItems();
    // }
  }

  public ifInside(): boolean {
    // TODO Testing and inspection
    // return true;
    if (null != this.menuContainer) return this.menuContainer.ifInside();
    else return false;
  }
  public isSaved(): boolean {
    return false;
  }
  public activateMenu() {
    this._isMenuActive = true;
  }
  public deactivateMenu() {
    this._isMenuActive = false;
  }
  public isMenuActive(): boolean {
    // TODO Testing and inspection
    return true;
    // return this._isMenuActive;
  }
  public getMenuSections(): MenuSection[] {
    return this.menuContainer.getMenuSections();
  }
  // public menuClick() {
  //   this.menu.doMenuAction();
  // }
}
