//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
// import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
// import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
// import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
// import { IColored } from 'app/interfaces/IColored.interface';
// import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { MenuItem } from 'app/models/ui/MenuItem.model';
// import { ColorTheme } from 'app/uimodels/ColorTheme.model';

@Component({
  selector: 'neocom-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {
  @Input() menu: MenuItem;

  constructor(protected appModelStore: AppModelStoreService) { }
  ngOnInit() {
  }
  public getMenuItemIcon(): string {
    return "/assets/res-sde/drawable/research.png";
  }
  public menuClick() {
    this.menu.doMenuAction();
  }
}
