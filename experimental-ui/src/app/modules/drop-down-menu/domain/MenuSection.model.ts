//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms
//               , the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code maid in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
import { EVariant } from 'app/interfaces/EVariant.enumerated';
import { IIconReference } from 'app/interfaces/IIconReference.interface';
import { AssetGroupIconReference } from 'app/interfaces/IIconReference.interface';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
// import { NeoComNode } from './NeoComNode.model';
// import { Separator } from './Separator.model';

export class MenuSection /*implements IMenuItem*/ {
  // public labelFlag: boolean = true;
  public title: string = "-MENU ITEM-";
  // private _iconReference: IIconReference = null;
  private _menuAction: Function = null;
  public menuItems: IMenuItem[] = [];

  //--- GETTERS & SETTERS
  // public getGroupIconReference(): string {
  //   if (this.hasIcon())
  //     return this._iconReference.getReference();
  //   else return "/assets/res-ui/drawable/defaulticonplaceholder.png";
  // }
  public setTitle(newtitle: string): MenuSection {
    this.title = newtitle;
    return this;
  }
  // public setLabelFlag(newflag: boolean): MenuSection {
  //   this.labelFlag = newflag;
  //   return this;
  // }
  // public hasIcon(): boolean {
  //   if (null == this._iconReference) return false;
  // }
  // public setIconReference(iconref: IIconReference): MenuSection {
  //   this._iconReference = iconref;
  //   return this;
  // }
  public setMenuAction(action: Function): MenuSection {
    this._menuAction = action;
    return this;
  }
  public addMenuItem(item: IMenuItem): MenuSection {
    this.menuItems.push(item);
    return this;
  }
}
