// - CORE
// - DOMAIN
import { IMenuBar } from "./interfaces/IMenuBar.interface";
import { MenuSection } from "./MenuSection.model";

export class MenuBar implements IMenuBar {
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

    // - I M E N U B A R   I N T E R F A C E
    isMenuVisible(): boolean {
        throw new Error("Method not implemented.");
    }
    getMenuSupporter(): IMenuBar {
        throw new Error("Method not implemented.");
    }
    getMenuSections(): MenuSection[] {
        throw new Error("Method not implemented.");
    }
    defineMenu(): void {
        throw new Error("Method not implemented.");
    }
    
    //--- IMENUBAR INTERFACE
    panelEnter(): void { }
    panelExit(): void { }
    ifInside(): boolean {
        return true;
    }
    hasMenu(): boolean { return true; }
    activateMenu(): void { }
    toggleMenu(): boolean { return true; }
    isMenuExpanded(): boolean { return true; }
    setMenuFlag(newflag: boolean): boolean { return true; }
}
