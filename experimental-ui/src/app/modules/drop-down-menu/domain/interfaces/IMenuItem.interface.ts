// - DOMAIN
import { MenuItem } from '../MenuItem.model';

export interface IMenuItem {
    title: string;
    setTitle(newtitle: string): MenuItem;
    doMenuAction();
}
