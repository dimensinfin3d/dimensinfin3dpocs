// - DOMAIN
import { MenuSection } from '../MenuSection.model';

export interface IMenuBar {
    isMenuVisible(): boolean;
    panelEnter(): void;
    panelExit(): void;
    ifInside(): boolean;
    getMenuSupporter(): IMenuBar;
    getMenuSections(): MenuSection[];
    defineMenu(): void;
}
