//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms
//               , the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code maid in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
import { IIconReference } from 'app/interfaces/IIconReference.interface';
//--- MODELS

export class MenuItem /*implements IMenuItem*/ {
  /** Menu text title. */
  public title: string = "-MENU ITEM-";
  /** State for the check status for this menu item. */
  public isActive: boolean = false;
  /** Pointer to the icon to show on the menu item. */
  private _iconReference: IIconReference = null;
  /** The action to do when the menu item is selected. */
  private _menuAction: Function = null;

  //--- G E T T E R S   &   S E T T E R S
  public getTitle(): string {
    return this.title;
  }
  public getIconReference(): string {
    if (this.hasIcon())
      return this._iconReference.getReference();
    else return "/assets/res-ui/drawable/defaulticonplaceholder.png";
  }
  public getActiveState(): boolean {
    return this.isActive;
  }
  public setTitle(newtitle: string): MenuItem {
    this.title = newtitle;
    return this;
  }
  public setIconReference(iconref: IIconReference): MenuItem {
    this._iconReference = iconref;
    return this;
  }
  public hasIcon(): boolean {
    if (null == this._iconReference) return false;
    else return true;
  }
  public setActiveState(state: boolean): MenuItem {
    this.isActive = state;
    return this;
  }
  public setMenuAction(action: Function): MenuItem {
    this._menuAction = action;
    return this;
  }
  public doMenuAction() {
    if (null != this._menuAction) this._menuAction(this);
  }
}
