//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
// import { IViewer } from 'app/interfaces/IViewer.interface';
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
// import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
// import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
// import { IColored } from 'app/interfaces/IColored.interface';
// import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- COMPONENTS
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
//--- MODELS
// import { NeoComNode } from 'app/models/NeoComNode.model';
// import { ColorTheme } from 'app/models/ui/ColorTheme.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';


/**
This NO TEMPLATE component is used to add Menu Bar functionality to another component. This allows to keep the separation from expandable to non-expandable components. This is the code component to add the menu to expandable components. We should differentiate from STANDARD and DETAILED renderizations.
*/
@Component({
  selector: 'notused-neocomnode',
  templateUrl: './notused.html'
})
export class ExpandableMenuBarNodeComponent extends NeoComNodeComponent implements IMenuBar {
  /** This flag is used to know if the *ngIf should render or not the Menu Bar. Menu bar is activated on hovering the target component. */
  private _menuVisible: boolean = true;
  /** This is the structure container for all the menu items. This stores directly the first level of menu sections. */
  protected _menuContents: MenuSection[] = [];

  //--- I M E N U B A R   I N T E R F A C E
  /** Returns the current state of the menu visibility state. */
  public isMenuVisible(): boolean {
    return this._menuVisible;
  }
  /** This method should be called then the cursor enters the panel osing the '(mouseenter)' event. */
  public panelEnter(): void {
    this._inside = true;
  }
  /** This methods is the one called when the cursor exists the panel. This shuld be detected with the '(mouseleave)' event. */
  public panelExit(): void {
    this._inside = false;
  }
  /** Returns 'true' while the cursor is inside the panel. */
  public ifInside(): boolean {
    return this._inside;
  }
  /** Once a component inherits from this class is can be supposed to be a Menu Supporter because it can generate and show a Menu Bar. So this allows to return self to get that feature instance. */
  public getMenuSupporter(): IMenuBar {
    return this;
  }
  /** Return the list of Menu Sections that should be rendered inside the Manu Bar for this panel. */
  public getMenuSections(): MenuSection[] {
    return this._menuContents;
  }
  public defineMenu(): void{}
}
